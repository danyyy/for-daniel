import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEventType,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Post } from '../posts/Post.model';
import { map, take, tap, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PostService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  //una funcio que realise lo mismo que todo el codigo de abajo resumido
  fetchPosts() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPost(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Leela',
        }),
        observe: 'body',
      }
    );
  }
  clearPosts() {
    this.http
      .delete('https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json', {
        observe: 'events',
        responseType: 'text',
      })
      .pipe(
        tap((response) => {
          if (response.type === HttpEventType.Sent) {
            console.log('request sent');
          }

          if (response.type === HttpEventType.Response) {
            console.log(response);
          }
        })
      )
      .subscribe((response) => {

      });
  }

  

//una funcion para utilizar varios posts 
  fetchPosts2() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        }),
        take(1),
        switchMap((posts) => {
          return this.authService.user.pipe(
            take(1),
            map((user) => {
              return { posts, user };
            })
          );
        }),
        tap((res) => {
          console.log(res);
        })
      );
    }
 //una funcion para crear varios posts
  createPost2(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Leela',
        }),
        observe: 'body',
      }
    );
  }
  clearPosts2() {
    this.http
      .delete('https://loginforo-7fee4-default-rtdb.firebaseio.com/posts.json', {
        observe: 'events',
        responseType: 'text',
      })
      .pipe(
        tap((response) => {
          if (response.type === HttpEventType.Sent) {
            console.log('request sent');
          }

          if (response.type === HttpEventType.Response) {
            console.log(response);
          }
        })
      )
      .subscribe((response) => {

      });
  }

}






