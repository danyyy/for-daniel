import { HttpClient } from '@angular/common/http';
import { ExpansionCase } from '@angular/compiler';
import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { Editor, Toolbar } from 'ngx-editor';

import { PostService } from '../services/post.service';
import { Post } from './Post.model';
//import { ToolbarService, LinkService, ImageService, HtmlEditorService } from '@syncfusion/ej2-angular-richtexteditor';
@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  postForm: FormGroup;
  posts: Post[];
  error = null;

  html = '<b>Hola</b>';

  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];



  constructor(private postService: PostService) {

    this.editor = new Editor();
  }

  ngOnInit(): void {
    this.postForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      content: new FormControl(null, Validators.required),
    });
    this.getPosts();
  }

  Guardar():void {
    console.log(this.html);
  }


  getPosts() {
    this.postService.fetchPosts().subscribe(
      (response) => {
        this.posts = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }

  onCreatePost() {
    const postData: Post = this.postForm.value;
    this.postService.createPost(postData).subscribe((response) => {
      console.log(response);
      this.getPosts();
    });
  }

  onClearPosts(event: Event) {
    event.preventDefault();
    this.postService.clearPosts();
    this.posts = [];
  }
}




//  export class text implements OnInit {
//   constructor() {}
//   title = 'angular-richtexteditor';
//   public mode: string = 'Markdown';
//   @ViewChild('exampleRTE')
//   public componentObject! : RichTextEditorComponent;
//   private buttonElement! : HTMLElement | null;
//   private htmlContent! : string;

//   getFormattedContent() {
//     this.buttonElement = document.getElementById('button');
//     this.htmlContent = this.componentObject.getHtml();
//   }

//   public customToolbar: Object ={
//     items: [ 'Bold', 'Italic','Undo', 'Redo', 'CreateTable', 'Image', 'CreateLink']
//   };
  
// }






